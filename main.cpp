#include <iostream>
#include <ucm_random>
using namespace std;

// https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797

#define RESET "\033[0m"
#define WHITE_ON_RED "\033[41;37m"

void printBorder(int size) {
    for (int i = 0; i < size; i++) {
        cout << WHITE_ON_RED << "         " << RESET;
        if (i < size - 1) {
            cout << " ";
        }
    }
    cout << endl;
}

void printTop(int dice[], int size) {
    for (int i = 0; i < size; i++) {
        if (dice[i] == 1) {
            cout << WHITE_ON_RED << "         " << RESET;
        } else if (dice[i] == 2 || dice[i] == 3) {
            cout << WHITE_ON_RED << "  *      " << RESET;
        } else {
            cout << WHITE_ON_RED << "  *   *  " << RESET;
        }
        
        if (i < size - 1) {
            cout << " ";
        }
    }
    cout << endl;
}

void printMiddle(int dice[], int size) {
    for (int i = 0; i < size; i++) {
        if (dice[i] == 1 || dice[i] == 3 || dice[i] == 5) {
            cout << WHITE_ON_RED << "    *    " << RESET;
        } else if (dice[i] == 2 || dice[i] == 4) {
            cout << WHITE_ON_RED << "         " << RESET;
        } else {
            cout << WHITE_ON_RED << "  *   *  " << RESET;
        }

        if (i < size - 1) {
            cout << " ";
        }
    }
    cout << endl;
}

void printBottom(int dice[], int size) {
    for (int i = 0; i < size; i++) {
        if (dice[i] == 1) {
            cout << WHITE_ON_RED << "         " << RESET;
        } else if (dice[i] == 2 || dice[i] == 3) {
            cout << WHITE_ON_RED << "      *  " << RESET;
        } else {
            cout << WHITE_ON_RED << "  *   *  " << RESET;
        }
        
        if (i < size - 1) {
            cout << " ";
        }
    }
    cout << endl;
}

void printDice(int dice[], int size) {
    printBorder(size);
    printTop(dice, size);
    printMiddle(dice, size);
    printBottom(dice, size);
    printBorder(size);
}

bool allSame(int dice[], int size) {
    int first = dice[0];

    for (int i = 1; i < size; i++) {
        if (first != dice[i]) {
            return false;
        }
    }

    return true;
}

int main(){
    int rolls = 7;
    const int handSize = 2;

    int playerHand[handSize];
    int playerScore = 0;
    int playerRolls[rolls * handSize];
    int playerRollsCount = 0;

    int dealerHand[handSize];
    int dealerScore = 0;
    int dealerRolls[rolls * handSize];
    int dealerRollsCount = 0;

    
    RNG rng;

    // player's turn
    for (int i = 0; i < rolls; i++) {
        system("clear");

        // generating random numbers for dice
        for (int j = 0; j < handSize; j++) {
            int number = rng.get(1, 6);
            playerHand[j] = number;
            playerScore += number;

            playerRolls[playerRollsCount] = number;
            playerRollsCount++;
        }

        // print hand and current score
        cout << "Current Score: " << playerScore << endl << endl;
        printDice(playerHand, handSize);
        cout << endl;

        if (allSame(playerHand, handSize)) {
            playerScore = 0;
            break;
        }

        // ask player if they want to continue
        cout << "Continue ([Y]/N): ";
        string c;
        getline(cin, c);
        if (toupper(c[0]) == 'N') {
            break;
        }
    }

    // dealer's turn
    for (int i = 0; i < rolls; i++) {
        // dealer rolls their dice
        for (int j = 0; j < handSize; j++) {
            int number = rng.get(1, 6);
            dealerHand[j] = number;
            dealerScore += number;

            dealerRolls[dealerRollsCount] = number;
            dealerRollsCount++;
        }

        if (allSame(dealerHand, handSize)) {
            dealerScore = 0;
            break;
        }


        if (dealerScore >= playerScore) {
            break;
        }

    }

    system("clear");
    cout << endl << "Your score: " << playerScore << endl;
    printDice(playerRolls, playerRollsCount);
    cout << endl;

    cout << endl << "Dealer score: " << dealerScore << endl;
    printDice(dealerRolls, dealerRollsCount);
    cout << endl;

    if (playerScore > dealerScore) {
        cout << "You win!" << endl;
    } else if (playerScore < dealerScore) {
        cout << "You lose!" << endl;
    } else {
        if (playerScore == 0) {
            cout << "It's a tie!" << endl;
        } else {
            cout << "You lose!" << endl;
        }
    }

    return 0;
}